const Joi = require('joi');
const SchemaModel = require('../config/schema');

const storeName = require('../config/table').tableName;
const tableName = `${storeName}_sessions`;

const sessionSchema = {
    hashKey: 'sessionId',
    timestamps: true,
    schema: Joi.object({
        sessionId: Joi.string().alphanum(),
        expires: Joi.number(),
    }).optionalKeys('expires').unknown(true)
};

const Session = SchemaModel(sessionSchema, {
    tableName
});

module.exports = Session;