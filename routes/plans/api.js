const router = require('express').Router();
Plan = require('../../models/plan');
const Methods = require('../../methods/custom');
const jwt = require('jsonwebtoken');
const config = require('../../config/config');

const apiOperation = (req, res,crudMethod,optionsObj)=> {
	const bodyParams = Methods.initializer(req, Plan);
	crudMethod(bodyParams, optionsObj, (err, plan) => {
		console.log('\nplan details', plan);
		res.send(plan);
	})
}

// Here the roo path '/' is  '/api/plans/'
router.get('/', (req, res) => {
	res.send('This is the api route for plan management');
});

router.get('/getallplans', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					const table = decode.storeName+'_store_data';
					Plan.selectTable(table);
					Plan.query('plan',(err,plans)=>{
						//The error is already handled in the query method; but the error first approach is used here, so if we wan't to do the error handling
						//here instead of the query function, it will be easier to switch back
						res.send(plans);
					})
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

router.post('/getplan', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					const table = decode.storeName+'_store_data';
					Plan.selectTable(table);
					apiOperation(req, res, Plan.getItem);
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

router.post('/addplan', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
	var bodyParams=req.body;
	//console.log("suma: " +bodyParams);
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
				//const bodyParams = Methods.initializer(req, Plan);
				//console.log("bodyparams: "+ bodyParams.title);
				var plan1={}; var plan2={}; var plan3={};			
				plan1=bodyParams.p1;
				 plan2=bodyParams.p2;
				 plan3=bodyParams.p3;
				console.log("plan1"+ plan1.title);
				console.log("plan2"+ plan2.title);
				console.log("plan3"+ plan3.title);
				Plan.createItem(plan1, {
							table: decode.storeName+"_store_data",
							overwrite: false
						}, (err, plan) => {
							console.log('\nplan details', plan);
							//res.send(plan);
				});
																		
																							Plan.createItem(plan2, {
																									table: decode.storeName+"_store_data",
																										overwrite: false																																			}, (err, plan) => {
																									console.log('\nplan details', plan);																																																		});											
				Plan.createItem(plan3, {																																				table: decode.storeName+"_store_data",																																		overwrite: false																																	}, (err, plan) => {																		console.log('\nplan details', plan);
				res.send({"plan":plan, "status":"success"});
	});
				
				//res.send(result);
			}
        })
    }
    else{
        res.send("please send a token");
    }
});

router.put('/updateplan', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
				const table = decode.storeName+"_store_data";
				Plan.selectTable(table);
				apiOperation(req, res, Plan.updateItem);
				   
            }
        })
    }
    else{
        res.send("please send a token");
    }
});

router.delete('/deleteplan', (req, res) => {
	var token = req.body.token || req.headers['token'];	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					const table = decode.storeName+"_store_data";
					Plan.selectTable(table);
					//apiOperation(req, res, Plan.deleteItem);
				
			 const removeCallback = (err, removeData) => {
                    console.log('\nThe removed item data is...\n', removeData);
                    res.send(removeData);
                }
          
                // passing the conditional object here {} as second parameter
                                 Plan.deleteItem({
                                                 	collectionType: req.body.collectionType,
                                                 	                    collectionId: req.body.collectionId
                                                 	                                    }, {ReturnValues: 'ALL_OLD'} ,removeCallback);

			}
			})
		}
		else{
			res.send("please send a token");
		}
});

module.exports = router;
